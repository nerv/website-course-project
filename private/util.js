
const fs = require('fs')
const path = require('path')

/**
 * Loads models from a directory
 * @param {String} dir
 * @param {Connection} connection
 * @return {Object}
 */
exports.loadModels = function (dir, connection) {
  let models = {}
  for (const fileName of fs.readdirSync(dir)) {
    if (/^[A-Z]/.test(fileName)) {
      const getModel = require(path.join(dir, fileName))
      const field = path.basename(fileName, '.js')
      models[field] = getModel(connection)
    }
  }
  return models
}
