
const config = require('config')
const app = require('./app')

const {host, port} = config.get('server')

app.listen(port, _ => {
  console.log(`Server running at http://${host}:${port}/`)
})
