
module.exports = function (connection) {

  const schema = new connection.Schema({
    title: String,
    link: String
  }, {
    timestamps: true
  })

  schema.statics.all = function () {
    return this
      .find()
      .sort('createdAt')
  }

  return connection.define('Menu', schema)
}
