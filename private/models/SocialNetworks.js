
module.exports = function (connection) {

  const schema = new connection.Schema({
    title: String,
    link: String,
    className: String
  }, {
    timestamps: true
  })

  /**
   * @return {Promise}
   */
  schema.statics.all = function () {
    return this
      .find()
      .sort('createdAt')
  }

  return connection.define('SocialNetworks', schema)
}
