
module.exports = function (connection) {

  const schema = new connection.Schema({
    title: String,
    description: String,
    imageUrl: String,
    content: String
  }, {
    timestamps: true
  })

  /**
   * @return {Promise}
   */
  schema.statics.all = function () {
    return this
      .find({})
      .sort('updatedAt')
  }

  return connection.define('Articles', schema)
}
