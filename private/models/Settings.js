
module.exports = function (connection) {

  const schema = new connection.Schema(
    {
      key: String,
      value: String
    },
    {
      timestamps: true // createdAt, updatedAt
    }
  )
  /**
   * @return {Promise}
   */
  schema.statics.all = function () {
    return this
      .find()
      .sort('key')
  }
  /**
   * @return {Promise}
   */
  schema.statics.hash = async function () {
    const items = await this.find().lean().exec()
    return items.reduce((acc, item) => {
      acc[item.key] = item.value
      return acc
    }, {})
  }

  return connection.define('Settings', schema)
}
