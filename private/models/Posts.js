
module.exports = function (connection) {
  const schema = new connection.Schema({
    title: String,
    subtitle: String,
    imageUrl: String,
    content: String
  }, {
    timestamps: true
  })

  /**
   * @return {Promise}
   */
  schema.statics.all = function () {
    return this
      .find({})
      .sort('createdAt')
  }

  return connection.define('Posts', schema)
}
