
module.exports = async function (ctx, next) {
  ctx.state.menuItems = await ctx.models.Menu.all()
  ctx.state.settings = await ctx.models.Settings.hash()
  ctx.state.socialNetworks = await ctx.models.SocialNetworks.all()
  return next()
}
