
const forms = require('forms')

const {fields, widgets} = forms

exports.getMenuForm = function () {
  return forms.create({
    title: fields.string(),
    link: fields.string()
  })
}

exports.getSettingsForm = function () {
  return forms.create({
    key: fields.string(),
    value: fields.string({
      widget: widgets.textarea()
    })
  })
}

exports.getSocialNetworksForm = function () {
  return forms.create({
    title: fields.string(),
    link: fields.string(),
    className: fields.string()
  })
}

exports.getArticlesForm = function () {
  return forms.create({
    title: fields.string(),
    description: fields.string(),
    imageUrl: fields.string(),
    content: fields.string({
      widget: widgets.textarea()
    })
  })
}

exports.getPostsForm = function () {
  return forms.create({
    title: fields.string(),
    subtitle: fields.string(),
    imageUrl: fields.string(),
    content: fields.string({
      widget: widgets.textarea()
    })
  })
}
