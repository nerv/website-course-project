
const forms = require('./forms')

exports.get = {
  '/': async function (ctx) {
    ctx.render(__dirname, 'views/index')
  },
  // -----------------
  '/menu': async function (ctx) {
    ctx.state.items = await ctx.models.Menu.all()
    ctx.render(__dirname, 'views/menu/index')
  },
  '/menu/add': async function (ctx) {
    ctx.state.form = forms.getMenuForm()
    ctx.render(__dirname, 'views/common/form')
  },
  '/menu/edit/:id': async function (ctx) {
    const item = await ctx.models.Menu.findById(ctx.request.params.id)
    const form = forms.getMenuForm()
    ctx.state.form = form.bind(item)
    ctx.state.item = item
    ctx.render(__dirname, 'views/common/form')
  },
  '/menu/remove/:id': async function (ctx) {
    ctx.render(__dirname, 'views/common/confirmation')
  },
  // -----------------
  '/settings': async function (ctx) {
    ctx.state.items = await ctx.models.Settings.all()
    ctx.render(__dirname, 'views/settings/index')
  },
  '/settings/add': async function (ctx) {
    ctx.state.form = forms.getSettingsForm()
    ctx.render(__dirname, 'views/common/form')
  },
  '/settings/edit/:id': async function (ctx) {
    const item = await ctx.models.Settings.findById(ctx.request.params.id)
    const form = forms.getSettingsForm()
    ctx.state.form = form.bind(item)
    ctx.state.item = item
    ctx.render(__dirname, 'views/common/form')
  },
  '/settings/remove/:id': async function (ctx) {
    ctx.render(__dirname, 'views/common/confirmation')
  },
  // -----------------
  '/social-networks': async function (ctx) {
    ctx.state.items = await ctx.models.SocialNetworks.all()
    ctx.render(__dirname, 'views/social-networks/index')
  },
  '/social-networks/add': async function (ctx) {
    ctx.state.form = forms.getSocialNetworksForm()
    ctx.render(__dirname, 'views/common/form')
  },
  '/social-networks/edit/:id': async function (ctx) {
    const item = await ctx.models.SocialNetworks.findById(ctx.request.params.id)
    const form = forms.getSocialNetworksForm()
    ctx.state.form = form.bind(item)
    ctx.state.item = item
    ctx.render(__dirname, 'views/common/form')
  },
  '/social-networks/remove/:id': async function (ctx) {
    ctx.render(__dirname, 'views/common/confirmation')
  },
  // -----------------
  '/articles': async function (ctx) {
    ctx.state.items = await ctx.models.Articles.all()
    ctx.render(__dirname, 'views/articles/index')
  },
  '/articles/add': async function (ctx) {
    ctx.state.form = forms.getArticlesForm()
    ctx.render(__dirname, 'views/common/form')
  },
  '/articles/edit/:id': async function (ctx) {
    const item = await ctx.models.Articles.findById(ctx.request.params.id)
    const form = forms.getArticlesForm()
    ctx.state.form = form.bind(item)
    ctx.state.item = item
    ctx.render(__dirname, 'views/common/form')
  },
  '/articles/remove/:id': async function (ctx) {
    ctx.render(__dirname, 'views/common/confirmation')
  },
  // -----------------
  '/posts': async function (ctx) {
    ctx.state.items = await ctx.models.Posts.all()
    ctx.render(__dirname, 'views/posts/index')
  },
  '/posts/add': async function (ctx) {
    ctx.state.form = forms.getPostsForm()
    ctx.render(__dirname, 'views/common/form')
  },
  '/posts/edit/:id': async function (ctx) {
    const item = await ctx.models.Posts.findById(ctx.request.params.id)
    const form = forms.getPostsForm()
    ctx.state.form = form.bind(item)
    ctx.state.item = item
    ctx.render(__dirname, 'views/common/form')
  },
  '/posts/remove/:id': async function (ctx) {
    ctx.render(__dirname, 'views/common/confirmation')
  }
}

exports.post = {
  '/menu/add': async function (ctx) {
    await ctx.models.Menu.create(ctx.request.body)
    ctx.redirect(ctx.mountPath)
  },
  '/menu/edit/:id': async function (ctx) {
    await ctx.models.Menu.findByIdAndUpdate(ctx.request.params.id, ctx.request.body)
    ctx.redirect(ctx.mountPath)
  },
  '/menu/remove/:id': async function (ctx) {
    await ctx.models.Menu.findByIdAndRemove(ctx.request.params.id)
    ctx.redirect(ctx.mountPath)
  },
  // -----------------
  '/settings/add': async function (ctx) {
    await ctx.models.Settings.create(ctx.request.body)
    ctx.redirect(ctx.mountPath)
  },
  '/settings/edit/:id': async function (ctx) {
    await ctx.models.Settings.findByIdAndUpdate(ctx.request.params.id, ctx.request.body)
    ctx.redirect(ctx.mountPath)
  },
  '/settings/remove/:id': async function (ctx) {
    await ctx.models.Settings.findByIdAndRemove(ctx.request.params.id)
    ctx.redirect(ctx.mountPath)
  },
  // -----------------
  '/social-networks/add': async function (ctx) {
    await ctx.models.SocialNetworks.create(ctx.request.body)
    ctx.redirect(ctx.mountPath)
  },
  '/social-networks/edit/:id': async function (ctx) {
    await ctx.models.SocialNetworks.findByIdAndUpdate(ctx.request.params.id, ctx.request.body)
    ctx.redirect(ctx.mountPath)
  },
  '/social-networks/remove/:id': async function (ctx) {
    await ctx.models.SocialNetworks.findByIdAndRemove(ctx.request.params.id)
    ctx.redirect(ctx.mountPath)
  },
  // -----------------
  '/articles/add': async function (ctx) {
    await ctx.models.Articles.create(ctx.request.body)
    ctx.redirect(ctx.mountPath)
  },
  '/articles/edit/:id': async function (ctx) {
    await ctx.models.Articles.findByIdAndUpdate(ctx.request.params.id, ctx.request.body)
    ctx.redirect(ctx.mountPath)
  },
  '/articles/remove/:id': async function (ctx) {
    await ctx.models.Articles.findByIdAndRemove(ctx.request.params.id)
    ctx.redirect(ctx.mountPath)
  },
  // -----------------
  '/posts/add': async function (ctx) {
    await ctx.models.Posts.create(ctx.request.body)
    ctx.redirect(ctx.mountPath)
  },
  '/posts/edit/:id': async function (ctx) {
    await ctx.models.Posts.findByIdAndUpdate(ctx.request.params.id, ctx.request.body)
    ctx.redirect(ctx.mountPath)
  },
  '/posts/remove/:id': async function (ctx) {
    await ctx.models.Posts.findByIdAndRemove(ctx.request.params.id)
    ctx.redirect(ctx.mountPath)
  }
}
