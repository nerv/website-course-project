/**
 * @return {Function}
 */
exports.errorHandler = function () {
  return async function (ctx, next) {
    try {
      await next()
    } catch (err) {
      ctx.app.emit('error', err, ctx)

      ctx.status = err.status

      if (!err.expose) {
        return
      }

      // @see https://github.com/jshttp/accepts#simple-type-negotiation
      switch (ctx.accepts('json', 'html')) {
        case 'json':
          ctx.type = 'json'
          ctx.body = {message: err.message}
          break
        case 'html':
          ctx.type = 'html'
          ctx.body = err.message
          break
        default:
          ctx.type = 'text'
          ctx.body = err.message
      }
    }
  }
}
