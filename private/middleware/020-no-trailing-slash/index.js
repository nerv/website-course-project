
const noTrailingSlash = require('koa-no-trailing-slash')

module.exports = noTrailingSlash()
