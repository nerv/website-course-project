
const path = require('path')
const nunjucks = require('nunjucks')

class Renderer {
  /**
   * @param {String|Array.<String>} path
   * @param {Object} [nunjucksOptions]
   */
  constructor (path, nunjucksOptions) {
    this.env = new nunjucks.Environment(
      new nunjucks.FileSystemLoader(path),
      Object.assign({}, nunjucksOptions)
    )
  }
  /**
   * @param {Object} ctx
   * @param {String} dir
   * @param {String} filePath
   * @param {Object} locals
   * @returns {String}
   */
  renderToString (ctx, dir, filePath, locals) {
    return this.env.render(path.join(dir, filePath + '.njk'), Object.assign({}, ctx.state, locals))
  }
  /**
   * @param {Object} ctx
   * @param {String} dir
   * @param {String} filePath
   * @param {Object} locals
   * @returns {String}
   */
  render (ctx, dir, filePath, locals) {
    ctx.response.type = 'html'
    ctx.response.body = this.renderToString(ctx, dir, filePath, locals)
  }
  /**
   * @returns {Function}
   */
  middleware () {
    return (ctx, next) => {
      // exposing
      ctx.renderer = this
      /**
       * @param {String} dir
       * @param {String} filePath
       * @param {Object} [locals]
       * @returns {String}
       */
      ctx.renderToString = this.renderToString.bind(this, ctx)
      /**
       * @param {String} dir
       * @param {String} filePath
       * @param {Object} [locals]
       */
      ctx.render = this.render.bind(this, ctx)
      // Go next
      return next()
    }
  }
}

/**
 * @param {String} path
 * @param {Object} [nunjucksOptions]
 */
module.exports = function (path, nunjucksOptions) {
  const renderer = new Renderer(path, nunjucksOptions)
  return renderer.middleware()
}

module.exports.Renderer = Renderer
