
const config = require('config')
const renderer = require('./renderer')

module.exports = renderer(
  config.get('views.root'),
  config.get('views.options')
)

// ctx.renderer
// ctx.render(__dirname, 'views/index', [locals])
// ctx.renderToString(__dirname, 'views/index', [locals])
