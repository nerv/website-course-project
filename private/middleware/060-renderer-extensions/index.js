
const moment = require('moment')

module.exports = function (ctx, next) {
  ctx.renderer.env.addGlobal('ctx', ctx)
  ctx.renderer.env.addFilter('date', date)
  ctx.renderer.env.addGlobal('sinceYear', sinceYear)
  return next()
}

function date (date_, format = 'DD MMMM YYYY') {
  return moment(date_).format(format)
}

function sinceYear (year1 = new Date().getFullYear()) {
  let year2 = new Date().getFullYear()
  return year1 < year2 ? `${year1}-${year2}` : String(year1)
}
