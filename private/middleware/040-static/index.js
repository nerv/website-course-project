
const config = require('config')
const serve = require('koa-static')

module.exports = serve(config.get('static.root'))
