
exports.get = {
  '/': async function (ctx) {
    ctx.state.posts = await ctx.models.Posts.all()
    ctx.render(__dirname, 'views/index')
  },
  '/articles/:id': async function (ctx) {
    ctx.state.item = await ctx.models.Articles.findById(ctx.request.params.id)
    ctx.render(__dirname, 'views/article')
  },
  '/contact': async function (ctx) {
    ctx.render(__dirname, 'views/contact')
  },
  '/posts/:id': async function (ctx) {
    ctx.state.item = await ctx.models.Posts.findById(ctx.request.params.id)
    ctx.render(__dirname, 'views/post')
  }
}
