
const mongoose = require('mongoose')
const pluginDefaultValues = require('mongoose-default-values')

// Use native promises
// http://eddywashere.com/blog/switching-out-callbacks-with-promises-in-mongoose/
mongoose.Promise = global.Promise

module.exports = class Connection {
  /**
   * @param {Object} options
   */
  constructor (options) {
    this.options = options
    this.Schema = mongoose.Schema
    this.SchemaType = mongoose.SchemaType
    this.SchemaTypes = mongoose.SchemaTypes
    this.mongooseConnection = null
    this.open()
  }
  /**
   *
   */
  open () {
    const {connection: con, connectionOptions: opts} = this.options
    // http://mongodb.github.io/node-mongodb-native/2.2/tutorials/connect/
    const uri = Connection.formatConnectionUri(con)
    this.mongooseConnection = mongoose.createConnection(uri, opts)
    this.mongooseConnection.on('open', this.onEvent.bind(this, 'opened'))
    this.mongooseConnection.on('close', this.onEvent.bind(this, 'closed'))
    this.mongooseConnection.on('close', process.exit)

    // Mongoose plugins
    mongoose.plugin(pluginDefaultValues)
  }
  /**
   *
   */
  close () {
    this.mongooseConnection.close()
  }
  /**
   * @param {String} action
   * @callback
   */
  onEvent (action) {
    const {connection: con} = this.options
    const opts = Object.assign({}, con)
    delete opts.password
    const connectionUriWithoutPassword = Connection.formatConnectionUri(opts)
    console.log('Connection with %s was %s', connectionUriWithoutPassword, action)
  }
  /**
   * @param {String} name
   * @param {String} schema
   * @return {Mongoose.Model}
   */
  define (name, schema) {
    return this.mongooseConnection.model(name, schema)
  }
  /**
   * Notice: by default mongodb has no enabled access control, so there is no default user or password.
   * @see http://gitlab.element-studio.ru:8008/npm/mongoose-connection/blob/master/lib/utils.js
   * @see http://mongodb.github.io/node-mongodb-native/2.2/tutorials/connect/
   * @see http://stackoverflow.com/a/38921949
   * @see https://docs.mongodb.com/v3.2/security/
   * @see https://docs.mongodb.com/v3.2/reference/configuration-options/#security.authorization
   * @param {Object} options
   * @return {string}
   */
  static formatConnectionUri (options) {
    const {host, port, database, username, password} = options
    const authentication = username
      ? password
        ? `${username}:${password}@`
        : `${username}@`
      : ''
    return `mongodb://${authentication}${host}:${port}/${database}`
  }
}
