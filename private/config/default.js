
const path = require('path')
const moment = require('moment')

moment.locale('ru')

module.exports = {
  database: {
    connection: {
      host: 'ds229418.mlab.com',
      database: 'website-course-project',
      username: 'developer',
      password: '8c42f519f3375bfc202ad869456106c8',
      port: 29418
    }
  },
  server: {
    host: 'localhost',
    port: 8080
  },
  static: {
    root: path.join(__dirname, '../../public')
  },
  views: {
    root: path.join(__dirname, '..'),
    options: {
      autoescape: false
    }
  }
}
