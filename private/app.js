
const config = require('config')
const path = require('path')
const httpShutdown = require('http-shutdown')
const Koa = require('koa')
const {readMiddlewareAndRoutes} = require('koa-architect')
const Connection = require('./Connection')
const {loadModels} = require('./util')

class App extends Koa {
  constructor () {
    super()

    const middlewareFullPath = path.join(__dirname, 'middleware')
    for (const middleware of readMiddlewareAndRoutes(middlewareFullPath)) {
      this.use(middleware)
    }

    this.connection = this.context.connection = new Connection(config.get('database'))

    const modelsFullPath = path.join(__dirname, 'models')
    this.models = this.context.models = loadModels(modelsFullPath, this.connection)

    // this.on('open', _ => this.connection.open())
    this.on('close', _ => this.connection.close())
  }
  /**
   * Shorthand for:
   *
   *    http.createServer(app.callback()).listen(...)
   *
   * @param {Mixed} ...
   * @return {Server}
   * @api public
   */
  listen (...args) {
    const server = super.listen(...args)
    server.on('listening', _ => this.emit('open'))
    server.on('close', _ => this.emit('close'))
    process.on('SIGINT', _ => this.emit('close'))
    process.on('SIGTERM', _ => this.emit('close'))
    return httpShutdown(server)
  }
}

module.exports = new App()
