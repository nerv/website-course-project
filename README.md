# website-course-project


## Requirements
+ [Git](https://git-scm.com/downloads) `>=2.5`
+ [Node.js](https://nodejs.org/en/download/) `>=8`


## Installation
```bash
npm install
```

## Running
```bash
npm run server
```

## Technologies / Tools
+ [Git](https://git-scm.com/) -- Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. 
+ [Node.js](https://nodejs.org/en/) -- Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine. Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient. 
+ [Bootstrap](https://getbootstrap.com/) -- Build responsive, mobile-first projects on the web with the world's most popular front-end component library. 
+ [Bootstrap: Clean blog theme](https://startbootstrap.com/template-overviews/clean-blog/) -- a theme for Bootstrap framework 
+ [Koa](http://koajs.com) -- next generation web framework for Node.js 
+ [MongoDB](https://www.mongodb.com/what-is-mongodb) -- MongoDB is a document database with the scalability and flexibility 
+ [Mongoose](http://mongoosejs.com/) -- elegant MongoDB object modeling for Node.js
+ [mlab](https://mlab.com/) -- mLab is the leading Database-as-a-Service for MongoDB
